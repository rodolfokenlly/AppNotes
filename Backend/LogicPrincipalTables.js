//#region modulos npm y variables

const stringify = require('json-stringify');
const Capadatos = require('../Backend/accesoData');

//#endregion modulos npm y variables


//#region Procesos Generales

function ProcesosGenerales(socket) {
    SeleccionarNotas(socket);
    InsertarNota(socket);
    EliminarNota(socket);
    
    SeleccionarCategorias(socket);
    InsertarCategoria(socket);
}

//#endregion Procesos Generales


//#region Operaciones Notas

function SeleccionarNotas(socket) {
    socket.on('selectNotas', (data, callback) => {
        Capadatos.EjecutarProcesoMongoDB(3, 'Notas', data, (resp) => {
            let respuesta = JSON.parse(resp);
            if (respuesta.hasOwnProperty('Error')) {
                callback(stringify({Error: respuesta.Error}));
                return;
            } else {
                callback(stringify(respuesta.data));
            }
        });
    });
}

function InsertarNota(socket) {
    socket.on('insertNota', (data, callback) => {
        const proceso = parseInt(data.Proceso);
        if (proceso == 0) { //insert
            const pipeline = [
                {
                    $sort: {
                        IdNota: -1
                    }
                },
                {
                    $limit: 1
                }
            ];

            Capadatos.EjecutarProcesoMongoDB(5, 'Notas', pipeline, (resp) => {
                const respuesta = JSON.parse(resp);
                if (respuesta.hasOwnProperty('Error')) {
                    callback(stringify({Error: respuesta.Error}));
                    return;
                } else {
                    let IdNotaNueva = 0;
                    const resptaFiltros = respuesta.data;
                    if (resptaFiltros.length > 0) {
                        IdNotaNueva = parseInt(resptaFiltros[0].IdNota + 1);
                        const nota = {
                            IdNota : IdNotaNueva,
                            Titulo : data.Titulo,
                            Descripcion : data.Descripcion,
                            Categoria : data.Categoria,
                            FechaRegistro : data.FechaRegistro,
                            Favorito : data.Favorito,
                            Estado : data.Estado
                        };
                        
                       Capadatos.EjecutarProcesoMongoDB(1, 'Notas', nota, (resp2) => {
                            const respuesta2 = JSON.parse(resp2);
                            if (respuesta2.hasOwnProperty('Error')) {
                                callback(stringify({Error: respuesta2.Error}));
                                return;
                            } else {

                                Capadatos.EjecutarProcesoMongoDB(3, 'Notas', {IdNota:nota.IdNota}, (resp3) => {
                                    const respuesta3 = JSON.parse(resp3);
                                    if (respuesta3.hasOwnProperty('Error')) {
                                        callback(stringify({Error: respuesta3.Error}));
                                        return;
                                    } else {
                                        const notaWatchEmit = {
                                            accion: '-Nota insertada Watch-',
                                            data: respuesta3.data[0]
                                        };
                                        socket.emit('notaInsertada', stringify(notaWatchEmit));
                                        socket.broadcast.emit('notaInsertada', stringify(notaWatchEmit));
                                        callback(stringify(respuesta3.data[0]));
                                    }
                                });
                            }
                        });
                    }
                }
            });

        }
        else if (proceso == 1) { //update
            const dataUpdate = {
                IdNota: parseInt(data.IdNota),
                filters: {
                    IdNota: parseInt(data.IdNota)
                },
                setUpdate: {
                    Titulo: data.Titulo,
                    Descripcion: data.Descripcion,
                    Categoria: data.Categoria,
                    Favorito: data.Favorito,
                }
            }

            Capadatos.EjecutarProcesoMongoDB(2, 'Notas', dataUpdate, (resp) => {
                const respuesta = JSON.parse(resp);
                if (respuesta.hasOwnProperty('Error')) {
                    callback(stringify({Error: respuesta.Error}))
                    return;
                } else {
                    const notaWatchEmit = {
                        accion: '-Nota actualizada Watch-',
                        data: respuesta.data[0]
                    };
                    socket.emit('notaActualizada', stringify(notaWatchEmit));
                    socket.broadcast.emit('notaActualizada', stringify(notaWatchEmit));
                    callback(stringify(respuesta.data[0]));
                }
            });
        }        
    });
}

function EliminarNota(socket) {
    socket.on('eliminarNota', (data, callback) => {
        const proceso = parseInt(data.Proceso);
        if (proceso == 4) {
            const dataDelete = {
                IdNota: data.IdNota,
                filters: {
                    IdNota: parseInt(data.IdNota)
                }
            }

            Capadatos.EjecutarProcesoMongoDB(4, 'Notas', dataDelete, (resp) => {
                const respuesta = JSON.parse(resp);
                if (respuesta.hasOwnProperty('Error')) {
                    callback(stringify({Error: respuesta.Error}));
                    return;
                } else {
                    const notaWatchEmit = {
                        accion: '-Nota eliminada Watch-',
                        data: {
                            IdNota: parseInt(respuesta.data)
                        }
                    };
                    socket.emit('notaEliminada', stringify(notaWatchEmit));
                    socket.broadcast.emit('notaEliminada', stringify(notaWatchEmit));
                    callback(stringify({IdNota: parseInt(respuesta.data)}))
                }
            });
        }
    });
}

//#endregion Operaciones Notas


//#region Operaciones Categorias

function SeleccionarCategorias(socket) {
    socket.on('selectCategorias', (data, callback) => {
        Capadatos.EjecutarProcesoMongoDB(3, 'Categoria', data, (resp) => {
            const respuesta = JSON.parse(resp);
            if (respuesta.hasOwnProperty('Error')) {
                callback(stringify({Error: respuesta.Error}));
                return;
            } else {
                callback(stringify(respuesta.data));
            }
        });
    });
}

function InsertarCategoria(socket) {
    socket.on('insertCategoria', (data, callback) => {
        const filtro = [{
            $sort: {
                'IdCategoria': -1
            },
            $limit: {
                'IdCategoria': 1
            }
        }];

        Capadatos.EjecutarProcesoMongoDB(5, 'Categoria', filtro, (resp) => {
            const respuesta = JSON.parse(resp);
            if (respuesta.hasOwnProperty('Error')) {
                callback(stringify({Error: respuesta.Error}));
                return;
            } else {
                let IdCategoriaNueva = 0;
                const resptaFiltros = respuesta.data;
                if (resptaFiltros.length > 0) {
                    IdCategoriaNueva = parseInt(resptaFiltros[0].IdCategoria + 1);
                    const categoria = {
                        IdCategoria : IdCategoriaNueva,
                        Categoria : data.Categoria,
                        FechaRegistro : data.FechaRegistro,
                        Estado : data.Estado
                    };
                    
                    Capadatos.EjecutarProcesoMongoDB(1, 'Categoria', categoria, (resp2) => {
                        const respuesta2 = JSON.parse(resp2);
                        if (respuesta2.hasOwnProperty('Error')) {
                            callback(stringify({Error: respuesta2.Error}));
                            return;
                        } else {
                            callback(stringify(respuesta2));
                        }
                    });
                }
            }
        });
    });
}

//#endregion Operaciones Categorias


//#region module exports
module.exports = {
    ProcesosGenerales
};
//#endregion module exports
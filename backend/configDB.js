var MongoLocalCasa = {
    Server: 'mongodb://localhost:27017',
    Database: 'dbnotas'
};

var MongoLocalTrabajo = {
    Server: 'mongodb://10.10.90.40:27017',
    Database: 'dbnotas'
};

var MongoServer = {
    Server: 'mongodb://appnotes-lkrodiappprueba.1d35.starter-us-east-1.openshiftapps.com:8080',
    Database: 'dbnotas'
};

var Colecciones = {
    Notas: 'Notas',
    Categorias: 'Categorias',
    Opciones: 'Opciones'
};

module.exports = {
    MongoLocalCasa,
    MongoLocalTrabajo,
    MongoServer,
    Colecciones
};